import { Controller, Get, Param, Query } from '@nestjs/common';
import { HighscoresService } from './highscores.service';

@Controller('highscores')
export class HighscoresController {
  constructor(private readonly highscoresService: HighscoresService) {}

  @Get(':world')
  async getOverall(
    @Param('world') world: string,
    @Query('page') page = 1,
    @Query('limit') limit = 25,
    @Query('gamemode') gamemode = 'all',
    @Query('skill') skill = -1,
  ) {
    return await this.highscoresService.get(world, page, limit, gamemode, skill);
  }

  @Get(':world/count')
  async getCount(@Param('world') world: string) {
    return await this.highscoresService.getCount(world);
  }
}
