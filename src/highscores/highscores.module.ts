import { Module } from '@nestjs/common';
import { HighscoresController } from './highscores.controller';
import { HighscoresService } from './highscores.service';
import { WorldDataService } from 'src/world/world.service';

@Module({
  imports: [],
  controllers: [HighscoresController],
  providers: [WorldDataService, HighscoresService],
})
export class HighscoresModule {}
