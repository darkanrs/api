import { Injectable } from '@nestjs/common';
import { WorldDataService } from 'src/world/world.service';

@Injectable()
export class HighscoresService {
  constructor(private readonly worldData: WorldDataService) {}

  async getCount(world: string) {
    const count = await this.worldData.getCollection(world, 'highscores').countDocuments();
    return { count };
  }

  async get(world: string, page = 1, limit = 25, gamemode = 'all', skill = -1) {
    page = Number(page);
    limit = Number(limit);
    const startIndex = (page - 1) * limit;
    let filter = {};
    switch (gamemode) {
      case 'ironman':
        filter = { ironman: true };
        break;
      case 'regular':
        filter = { ironman: false };
        break;
      default:
        filter = {};
        break;
    }
    let sort: any = {};
    if (!skill || skill < 0) sort = { totalLevel: -1, totalXp: -1 };
    else {
      sort = {};
      sort['xp.' + skill] = -1;
    }
    return await this.worldData
      .getCollection(world, 'highscores')
      .find(filter, { projection: { _id: 0, username: 0 } })
      .sort(sort)
      .skip(startIndex)
      .limit(limit)
      .toArray();
  }
}
