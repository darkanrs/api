// import { HttpService } from '@nestjs/axios';
// import { Injectable } from '@nestjs/common';
// import fetch from 'node-fetch';
// import { Utils } from '../util';
// import { WorldDataService } from 'src/world/world.service';

// //Make a day range
// //Use ChartJS to make charts like Rune Metrics

// @Injectable()
// export class TemporalHiscoreService {
//   constructor(private readonly httpService: HttpService, private readonly worldData: WorldDataService) {}

//   async get(world: string, firstDayPast = 0, secondDayPast = 1, page = 1, limit = 6, skill = -1) {
//     firstDayPast = Number(firstDayPast);
//     secondDayPast = Number(secondDayPast);
//     if (firstDayPast >= secondDayPast) return { error: 'firstDayPast must be less than second day' };
//     page = Number(page);
//     limit = Number(limit);
//     skill = Number(skill);

//     const firstDate = new Date();
//     firstDate.setDate(firstDate.getDate() - firstDayPast);
//     let filter = { date: firstDate.toDateString() };
//     const firstHighscore = await this.worldData.getCollection(world, 'temporal').find(filter).toArray();
//     const firstSnapshot = firstHighscore[0]['snapshot'];

//     const secondDate = new Date();
//     secondDate.setDate(secondDate.getDate() - secondDayPast);
//     filter = { date: secondDate.toDateString() };
//     const daysBackHiscore = await this.worldData.getCollection(world, 'temporal').find(filter).toArray();
//     const daysBackSnapshot = daysBackHiscore[0]['snapshot'];
//     Object.keys(daysBackSnapshot);
//     const response = {
//       firstDayPast: firstDate.toDateString(),
//       secondDayPast: secondDate.toDateString(),
//       snapshot: [],
//     };

//     let snapshot = [];
//     Object.keys(firstSnapshot).forEach(function (key) {
//       const playerObject = {};
//       playerObject[key] = {
//         xpDifference: -1,
//         levelDifference: 0,
//       };
//       if (daysBackSnapshot[key] == undefined) {
//         snapshot.push(playerObject);
//         return;
//       }
//       if (skill == -1)
//         playerObject[key] = {
//           xpDifference: firstSnapshot[key].totalXp - daysBackSnapshot[key].totalXp,
//           levelDifference: firstSnapshot[key].totalLevel - daysBackSnapshot[key].totalLevel,
//         };
//       else
//         playerObject[key] = {
//           xpDifference: firstSnapshot[key].xp[skill] - daysBackSnapshot[key].xp[skill],
//           levelDifference:
//             Utils.getSkillLevelByXP(firstSnapshot[key].xp[skill], skill) -
//             Utils.getSkillLevelByXP(daysBackSnapshot[key].xp[skill], skill),
//         };
//       snapshot.push(playerObject);
//     });

//     snapshot.sort((a, b) => {
//       if (a[Object.keys(a)[0]].xpDifference == b[Object.keys(b)[0]].xpDifference) return 0;
//       if (a[Object.keys(a)[0]].xpDifference > b[Object.keys(b)[0]].xpDifference) return -1;
//       else return 1;
//     });

//     const startIndex = (page - 1) * limit;
//     snapshot = snapshot.slice(startIndex, startIndex + limit);
//     response['snapshot'] = snapshot;
//     return response;
//   }

//   async getPlayer(world: string, firstDayPast = 0, secondDayPast = 1, username = '') {
//     if (firstDayPast >= secondDayPast) return { error: 'first day must be less than second day' };
//     firstDayPast = Number(firstDayPast);
//     secondDayPast = Number(secondDayPast);
//     username = Utils.formatNameForDisplay(username);
//     const firstDayPastDate = new Date();
//     firstDayPastDate.setDate(firstDayPastDate.getDate() - firstDayPast);
//     const firstDay = firstDayPastDate.toDateString();
//     const date = new Date();
//     date.setDate(date.getDate() - secondDayPast);
//     const pastDate = date.toDateString();

//     const filter = { username: username };
//     let playerData: any /* GROSS */ = await this.worldData
//       .getCollection(world, 'temporalPlayer')
//       .find(filter)
//       .toArray();
//     if (playerData.length == 0) return {};
//     playerData = playerData[0];
//     const todaysData = playerData[firstDay];
//     const pastData = playerData[pastDate];

//     if (todaysData == undefined || pastData == undefined) return {};

//     const xpRanks = [];
//     const levelsUp = [];
//     let overallRank = -1;
//     let overallLevelsUp = -1;
//     for (let skill = -1; skill < 25; skill++) {
//       const response = await fetch(
//         `https://localhost:8443/v1/temporal?daysBack=${secondDayPast}&limit=99999&skill=${skill}`
//       );
//       const temporal = await response.json();
//       const snapshot = temporal['snapshot'];
//       for (let i = 0; i < snapshot.length; i++) {
//         const snapshotUser = Object.keys(snapshot[i])[0];
//         if (snapshotUser == username) {
//           if (skill == -1) {
//             overallRank = i + 1;
//             overallLevelsUp = snapshot[i][snapshotUser].levelDifference;
//             break;
//           }
//           xpRanks.push(i + 1);
//           levelsUp.push(snapshot[i][snapshotUser].levelDifference);
//           break;
//         }
//       }
//     }

//     const xpDifferential = [];
//     for (let i = 0; i < todaysData.xp.length; i++) xpDifferential[i] = todaysData.xp[i] - pastData.xp[i];
//     const differentialData = {
//       firstDayPast: firstDay,
//       secondDayPast: pastDate,
//       totalXp: todaysData.totalXP - pastData.totalXP,
//       totalLevel: todaysData.totalLevel - pastData.totalLevel,
//       xpDifferential: xpDifferential,
//       xpRanks: xpRanks,
//       levelsUp: levelsUp,
//       overallRank: overallRank,
//       overallLevelsUp: overallLevelsUp,
//     };
//     return differentialData;
//   }
// }
