import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AccountsService } from 'src/accounts/accounts.service';

@Injectable()
export class SocialService {
  constructor(
    private readonly accounts: AccountsService,
    private readonly config: ConfigService,
    private readonly httpService: HttpService,
  ) {}

  async messageFc(username: string, message: any) {
    const account = await this.accounts.getByUserOrEmail(username);
    if (!account) return;
    await this.httpService.axiosRef.post(
      'http://' + this.config.get<string>('LOBBY_API_URI') + 'sendmessage',
      {
        source: account,
        destination: {
          type: 'FRIENDS_CHAT',
          key: message.chatOwner,
        },
        message: message.message,
      },
      {
        headers: { key: this.config.get<string>('LOBBY_API_KEY') },
      },
    );
  }

  async broadcast(username: string, message: any) {
    let account = undefined;
    if (message.appendUser) {
      account = await this.accounts.getByUserOrEmail(username);
      if (!account) return;
    }
    await this.httpService.axiosRef.post(
      'http://' + this.config.get<string>('LOBBY_API_URI') + 'sendmessage',
      {
        source: account,
        destination: {
          type: 'BROADCAST',
        },
        message: message.message,
      },
      {
        headers: { key: this.config.get<string>('LOBBY_API_KEY') },
      },
    );
  }
}
