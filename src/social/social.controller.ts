import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { GetUser, RequireRights, Rights, RightsGuard } from 'src/auth/rights';
import { SocialService } from './social.service';

@Controller('social')
@UseGuards(JwtAuthGuard, RightsGuard)
export class SocialController {
  constructor(private readonly social: SocialService) {}

  @Post('message-fc')
  async messageFc(@Body() message: any, @GetUser('username') username: string) {
    return await this.social.messageFc(username, message);
  }

  @Post('broadcast')
  @RequireRights(Rights.ADMIN)
  async broadcast(@Body() message: any, @GetUser('username') username: string) {
    return await this.social.broadcast(username, message);
  }
}
