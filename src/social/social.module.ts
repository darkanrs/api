import { Module } from '@nestjs/common';
import { SocialController } from './social.controller';
import { SocialService } from './social.service';
import { AccountsService } from 'src/accounts/accounts.service';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import mongoose from 'mongoose';
import { SocialGateway } from './social.gateway';
import { JwtStrategy } from 'src/auth/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule, ConfigService } from '@nestjs/config';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Account',
          useFactory: (): any => new mongoose.Schema({}, { strict: false }),
          collection: 'accounts',
        },
      ],
      'lobbyConnection',
    ),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET'),
        signOptions: { expiresIn: '48h' },
      }),
    }),
  ],
  controllers: [SocialController],
  providers: [AccountsService, SocialService, JwtStrategy, SocialGateway],
})
export class SocialModule {}
