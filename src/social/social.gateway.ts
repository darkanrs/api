import {
  WebSocketGateway,
  OnGatewayInit,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketServer,
  MessageBody,
  ConnectedSocket,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { Server, WebSocket } from 'ws';
import { AccountsService } from 'src/accounts/accounts.service';
import { HttpService } from '@nestjs/axios';

@WebSocketGateway({ path: '/v1/social/ws', serveClient: false })
export class SocialGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  private static readonly logger = new Logger(SocialGateway.name);
  private masterSocket: WebSocket;
  private connectedUsers = new Set<any>();

  constructor(
    private readonly config: ConfigService,
    private readonly accounts: AccountsService,
    private readonly httpService: HttpService,
    private jwt: JwtService,
  ) {
    this.connectToMaster();
  }

  @WebSocketServer()
  server: Server;

  connectToMaster() {
    this.masterSocket = new WebSocket('ws://' + this.config.get<string>('LOBBY_API_URI') + 'subscribe', {
      headers: {
        key: this.config.get<string>('LOBBY_API_KEY'),
      },
    });

    this.masterSocket.on('open', () =>
      SocialGateway.logger.log(
        'Connected to lobby social websocket at ' + 'ws://' + this.config.get<string>('LOBBY_API_URI') + 'subscribe',
      ),
    );

    this.masterSocket.on('message', data => {
      const message = JSON.parse(data.toString());
      this.connectedUsers.forEach(client => {
        if (!client.authedUser) return;
        if (message.type == 'FCMessageEvent' && client.authedUser.social?.currentFriendsChat != message.data.fcOwner)
          return;
        client.send(data.toString());
      });
    });

    this.masterSocket.on('close', () => {
      SocialGateway.logger.log('Disconnected from lobby social websocket, attempting to reconnect...');
      setTimeout(() => this.connectToMaster(), 100);
    });
  }

  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  afterInit(server: any) {}

  async handleConnection(client: any, ...args: any[]) {
    let bearerToken = args[0].url
      ? new URL(args[0].url, `http://${args[0].headers.host}`).searchParams.get('token')
      : null;
    if (!bearerToken) bearerToken = args[0].headers?.authorization?.split(' ')[1];
    if (!bearerToken) return client.close(4004, 'Unauthorized Request');
    const decoded = this.jwt.verify(bearerToken, { ignoreExpiration: false }) as any;
    if (!decoded) return client.close(4004, 'Unauthorized Request');
    const account = await this.accounts.getChatInformation(decoded.username);
    client.authedUser = account;
    this.connectedUsers.add(client);
  }

  handleDisconnect(client: WebSocket) {
    this.connectedUsers.delete(client);
  }

  @SubscribeMessage('fc-message')
  async onMessage(@MessageBody() data: any, @ConnectedSocket() client: any) {
    try {
      const authedUser = client.authedUser;
      if (!authedUser || !client.authedUser.social?.currentFriendsChat) return client.close();

      await this.httpService.axiosRef.post(
        'http://' + this.config.get<string>('LOBBY_API_URI') + 'sendmessage',
        {
          source: authedUser,
          destination: {
            type: 'FRIENDS_CHAT',
            key: client.authedUser.social?.currentFriendsChat,
          },
          message: data.message,
        },
        {
          headers: { key: this.config.get<string>('LOBBY_API_KEY') },
        },
      );
    } catch (error) {
      SocialGateway.logger.error('Error handling message:', error);
    }
  }
}
