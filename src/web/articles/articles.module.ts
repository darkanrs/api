import { Module } from '@nestjs/common';
import { ArticlesController } from './articles.controller';
import { ArticlesService } from './articles.service';
import { HttpModule } from '@nestjs/axios';
import { MongooseModule } from '@nestjs/mongoose';
import { Article, ArticleSchema } from './schema/article.schema';

@Module({
  imports: [
    MongooseModule.forFeatureAsync(
      [
        {
          name: Article.name,
          useFactory: (): any => ArticleSchema,
          collection: 'news-articles',
        },
      ],
      'lobbyConnection',
    ),
    HttpModule,
  ],
  controllers: [ArticlesController],
  providers: [ArticlesService],
})
export class ArticlesModule {}
