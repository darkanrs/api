import { Module } from '@nestjs/common';
import { GEController } from './ge.controller';
import { GEService } from './ge.service';
import { WorldDataService } from 'src/world/world.service';

@Module({
  imports: [],
  controllers: [GEController],
  providers: [WorldDataService, GEService],
})
export class GEModule {}
