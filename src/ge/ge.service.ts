import { Injectable } from '@nestjs/common';
import { WorldDataService } from 'src/world/world.service';

@Injectable()
export class GEService {
  constructor(private readonly worldData: WorldDataService) {}

  async getAllOffers(world: string, page = 1, limit = 25, offerType?: string) {
    page = Number(page);
    limit = Number(limit);
    const startIndex = (page - 1) * limit;
    const filter = offerType ? { offerType } : {};
    return await this.worldData
      .getCollection(world, 'grandexchange')
      .find(filter, { projection: { _id: 0, box: 0, totalGold: 0, owner: 0, processedItems: 0 } })
      .skip(startIndex)
      .limit(limit)
      .toArray();
  }

  async getOffersByItem(world: string, page = 1, limit = 25, offerType?: string) {
    const startIndex = (page - 1) * limit;
    const filter = offerType ? { offerType } : {};
    return await this.worldData
      .getCollection(world, 'grandexchange')
      .find(filter, { projection: { _id: 0, box: 0, totalGold: 0, owner: 0, processedItems: 0 } })
      .skip(startIndex)
      .limit(limit)
      .toArray();
  }
}
