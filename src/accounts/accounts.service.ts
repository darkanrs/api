import { HttpService } from '@nestjs/axios';
import { BadRequestException, Injectable } from '@nestjs/common';
import { CreateAccountDto } from './dto/create-account';
import { Document, Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AccountsService {
  constructor(
    private readonly config: ConfigService,
    private readonly httpService: HttpService,
    @InjectModel('Account', 'lobbyConnection')
    private readonly accounts: Model<Document<any>>,
  ) {}

  async getByUserOrEmail(usernameOrEmail: string) {
    return await this.accounts.collection.findOne({ $or: [{ username: usernameOrEmail }, { email: usernameOrEmail }] });
  }

  async getChatInformation(usernameOrEmail: string) {
    return await this.accounts.collection.findOne(
      { $or: [{ username: usernameOrEmail }, { email: usernameOrEmail }] },
      {
        projection: {
          _id: 0,
          muted: 1,
          username: 1,
          displayName: 1,
          rights: 1,
          'social.currentFriendsChat': 1,
        },
      },
    );
  }

  async getByUserOrEmailSafe(usernameOrEmail: string) {
    return await this.accounts.collection.findOne(
      { $or: [{ username: usernameOrEmail }, { email: usernameOrEmail }] },
      {
        projection: {
          _id: 0,
          password: 0,
          prevPasswords: 0,
        },
      },
    );
  }

  async getByDisplayName(displayName: string) {
    return await this.accounts.collection.findOne(
      { displayName: displayName },
      {
        projection: {
          displayName: 1,
          rights: 1,
        },
      },
    );
  }

  async create(account: CreateAccountDto) {
    try {
      const response = await this.httpService.axiosRef.post(
        'http://' + this.config.get<string>('LOBBY_API_URI') + 'createaccount',
        account,
        {
          headers: { key: this.config.get<string>('LOBBY_API_KEY') },
        },
      );
      delete response.data.password;
      delete response.data.prevPasswords;
      return response.data;
    } catch (e) {
      throw new BadRequestException(e.response.data.error);
    }
  }
}
