import { SetMetadata, createParamDecorator } from '@nestjs/common';
import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

export enum Rights {
  PLAYER = 'PLAYER',
  MOD = 'MOD',
  ADMIN = 'ADMIN',
  DEVELOPER = 'DEVELOPER',
  OWNER = 'OWNER',
}

const PRIVILEGE_TIERS = new Map<Rights, number>([
  [Rights.PLAYER, 0],
  [Rights.MOD, 1],
  [Rights.ADMIN, 2],
  [Rights.DEVELOPER, 3],
  [Rights.OWNER, 4],
]);

export const RIGHTS_KEY = 'rights';
export const RequireRights = (...roles: Rights[]) => SetMetadata(RIGHTS_KEY, roles);

@Injectable()
export class RightsGuard implements CanActivate {
  constructor(private reflector: Reflector) {}

  canActivate(context: ExecutionContext): boolean {
    const requiredRights = this.reflector.get<Rights[]>(RIGHTS_KEY, context.getHandler());
    if (!requiredRights) return true;
    const request = context.switchToHttp().getRequest();
    return requiredRights.some(
      requiredRole => PRIVILEGE_TIERS.get(request.user.rights) || 0 >= (PRIVILEGE_TIERS.get(requiredRole) || 0),
    );
  }
}

export const GetUser = createParamDecorator((data: keyof any, ctx: ExecutionContext) => {
  const request = ctx.switchToHttp().getRequest();
  const user = request.user;
  return data ? user?.[data] : user;
});
