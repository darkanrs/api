import { Module } from '@nestjs/common';
import { PlayersController } from './players.controller';
import { PlayersService } from './players.service';
import { WorldDataService } from 'src/world/world.service';
import { MongooseModule } from '@nestjs/mongoose';
import mongoose from 'mongoose';

@Module({
  imports: [
    MongooseModule.forFeatureAsync(
      [
        {
          name: 'Account',
          useFactory: (): any => new mongoose.Schema({}, { strict: false }),
          collection: 'accounts',
        },
      ],
      'lobbyConnection',
    ),
  ],
  controllers: [PlayersController],
  providers: [WorldDataService, PlayersService],
})
export class PlayersModule {}
