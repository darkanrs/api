import { Controller, Get, Param, UseGuards } from '@nestjs/common';
import { PlayersService } from './players.service';
import { GetUser } from 'src/auth/rights';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('players')
export class PlayersController {
  constructor(private readonly playersService: PlayersService) {}

  @Get(':world')
  @UseGuards(JwtAuthGuard)
  async getCurrentlyAuthedUser(@GetUser('username') username: string, @Param('world') world: string) {
    return await this.playersService.getByUsername(world, username);
  }

  @Get(':world/:displayName')
  async getPlayerByDisplayName(@Param('world') world: string, @Param('displayName') displayName: string) {
    return await this.playersService.getByDisplay(world, displayName);
  }
}
