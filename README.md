# Darkan Database API
This API is written in Node Typscript using the NestJS framework. It is designed to manage the interplay between three servers: the Darkan web server, game server, and game lobby server. The lobby server is capable of managing multi-world functionality. It is also open source and usable by anyone who accesses its end points.

## Features
- GET requests for highscores, individual player data, account data, world Grand Exchange and news listings.
- POST requests for creating/editing news for the webserver in the same database, and creating/updating accounts.
- All new player and highscores data is managed by the game server.
- All account information is managed by the lobby server.
- GET requests have the ability to filter its requested data by parameters in the GET URL.

## Installation
To install the required packages, use the following command:
```bash
npm i
```

## Starting the API Server
To start the server, use the following command:
```bash
npm run start
```

## Usage
### GET Requests
- Highscores: **'/v1/highscores?:page&:limitinpage&:gamemode&:skill'**
- Grand Exchange: **'/v1/ge/buy?:page&:limit'**, **'/v1/ge/sell?:page&:limit'**
- Individual player data: **'/v1/players/:username'**
- Account data: *hidden*
- News listings: **'/v1/web/edit/:id'**, **'/v1/web?:page&:limit&:type'**

### POST Requests
- Create/edit news: **'/news/create-article'**
- Create/update accounts: *hidden*

## Notes
The lobby server manages user accounts and is capable of managing multi-world functionality but uses this API to interact with a lobby MongoDB. The game server manages new player and highscores data for each world. GET requests have the ability to filter data by parameters in the endpoint URL.

