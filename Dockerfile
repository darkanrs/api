FROM node:20-alpine

COPY --chown=node:node dist ./dist
COPY --chown=node:node node_modules ./node_modules

USER node

CMD [ "node", "dist/main.js"]